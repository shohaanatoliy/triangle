def is_triangle(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        return False

    # Перевірка на рівносторонній трикутник
    if a == b == c:
        return True

    # Перевірка на рівнобедрений трикутник
    if a == b or a == c or b == c:
        return True
        
    if a + b > c and a + c > b and b + c > a:
        return True
    else:
        return False
